import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzSpinModule } from 'ng-zorro-antd/spin';
import { LoginComponent } from './login/login.component';
import { NzCarouselModule } from 'ng-zorro-antd';
import { SignupComponent } from './signup/signup.component';


import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NzNotificationModule } from 'ng-zorro-antd/notification';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzRadioModule } from 'ng-zorro-antd/radio';
import { CommonModule } from '@angular/common';
import {NgxValidateModule} from 'ngx-validate';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NzSkeletonModule } from 'ng-zorro-antd/skeleton';
import { NzAlertModule } from 'ng-zorro-antd/alert';

import { PayComponent } from './dashboard/pay/pay.component';
import { LayoutComponent } from './dashboard/layout/layout.component';
import { BillsCardsComponent } from './dashboard/pay/bills-cards/bills.cards.component';
import { PaymentCardComponent } from './dashboard/pay/payment-card/payment.card.component';
import { AddBillComponent } from './dashboard/pay/add-bill/add.bill.component';
import { BillsDetailsComponent } from './dashboard/pay/bills-details/bills.details.component';

import { InterceptorService } from './provider/interceptor.service';
import {AuthenticateService} from './provider/authenticate.service';
import { BillsService } from './provider/bills.service';
import { ForgetPasswordComponent } from './forget-password/forget-password.component';
import { ResetFinishComponent } from './reset-finish/reset-finish.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SignupComponent,
    PayComponent,
    LayoutComponent,
    BillsCardsComponent,
    PaymentCardComponent,
    AddBillComponent,
    BillsDetailsComponent,
    ForgetPasswordComponent,
    ResetFinishComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NzButtonModule,
    NzCarouselModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NzSpinModule,
    NzNotificationModule,
    NzFormModule,
    NzToolTipModule,
    CommonModule,
    NgxValidateModule,
    NzSelectModule,
    BrowserAnimationsModule,
    NzRadioModule,
    NzSkeletonModule,
    NzAlertModule,
  ],
  exports: [NzToolTipModule],
  providers: [AuthenticateService,BillsService,BillsCardsComponent,LayoutComponent,
    { provide: HTTP_INTERCEPTORS, useClass: InterceptorService, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
