import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NzCarouselComponent, NzNotificationService } from 'ng-zorro-antd';
import { NgxValidators } from 'ngx-validate';
import { first } from 'rxjs/operators';
import { AuthenticateService } from '../provider/authenticate.service';


@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {

  @ViewChild("SingupCarousel") myCarousel: NzCarouselComponent;
  formSignupData;

  constructor(private authenticateService:AuthenticateService, private notification: NzNotificationService, public router: Router,) {
    this.formSignupData = new FormGroup({
      companyNameAr: new FormControl('', {validators: [Validators.required]}),
      companyNameEn: new FormControl('', [Validators.required]),
      name: new FormControl('', [Validators.required]),
      role: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required, Validators.email]),
      mobile: new FormControl('', [Validators.required, NgxValidators.isNumber]),
      bankAcount: new FormControl('', [Validators.required]),
      bankNumber: new FormControl('', [Validators.required]),
      iban: new FormControl('', [Validators.required]),
      vatnumber: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required])
   });
  }

  currentStep = 0;

  ngOnInit() {
  }
  changeSteps(data){
    console.log(data);
    this.currentStep = data.to;
  }
  goTo(step) {
    console.log(this.formSignupData);

    this.myCarousel.goTo(Number(step));
  }

  registerAction(){
    console.log(this.formSignupData.value);
    let body = {
      "companyNameAr": this.formSignupData.value.companyNameAr,
      "companyNameEn": this.formSignupData.value.companyNameEn,
      "email": this.formSignupData.value.email,
      "mobile": this.formSignupData.value.mobile+"",
      "bankName": this.formSignupData.value.bankAcount,
      "accountNumber": this.formSignupData.value.bankNumber+"",
      "iban": this.formSignupData.value.iban+"",
      "vatNumber": this.formSignupData.value.vatnumber+"",
      "password": this.formSignupData.value.password
    }
    this.authenticateService.registerSerives(body).pipe(first())
      .subscribe(
        response => {
          console.log(response);
          this.router.navigate(['login']);
          this.notification
          .blank(
            'Sign Up Sucessfuly',
            'Now you can login to Tag!',
            { nzDuration: 5 }
          );
        },
        error => {
          console.log(error);
          this.notification.blank(
            'Sing Up Error',
            error.error? error.error.title : 'Some Data Not Valid!',
            { nzDuration: 5 }
          )
          // this.loadingDocuments = false;
        }
      );
  }
}
