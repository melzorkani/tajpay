import { LayoutComponent } from './dashboard/layout/layout.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PayComponent } from './dashboard/pay/pay.component';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import {ForgetPasswordComponent} from "./forget-password/forget-password.component";
import {ResetFinishComponent} from "./reset-finish/reset-finish.component";

const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'singup', component: SignupComponent },
  { path: "dashboard", component: LayoutComponent},
  { path: "forget-password", component: ForgetPasswordComponent},
  { path: "reset-finish", component: ResetFinishComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
