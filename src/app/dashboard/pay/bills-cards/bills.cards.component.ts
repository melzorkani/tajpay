import { LayoutComponent } from './../../layout/layout.component';
import { Component, EventEmitter, Inject, Injectable, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { startWith, map, first } from 'rxjs/operators';
import { NzNotificationService } from 'ng-zorro-antd';
import { BillsService } from 'src/app/provider/bills.service';

@Component({
  selector: 'app-bills-cards',
  templateUrl: './bills.cards.component.html',
  styleUrls: ['./bills.cards.component.scss']
})
export class BillsCardsComponent implements OnChanges {

  isLoading = false;

  @Input() type;
  @Input() search;
  @Output() selectedcard = new EventEmitter<any>();

  billsData = [];

  constructor(public router: Router, private notification: NzNotificationService, private billsService: BillsService,
              private layoutComponent: LayoutComponent) {
  }
  ngOnChanges(changes: SimpleChanges): void {
    console.log(this.type);
    this.getTransaction(this.type, this.layoutComponent.searchValue);
  }

  OnChanges() {

  }

  public getTransaction(key , value) {
    this.isLoading = true;
    this.billsService.getTransaction(key, value).pipe(first())
    .subscribe(
      response => {
        console.log(response);
        this.billsData = response;
        console.log(this.billsData.length);

        this.isLoading = false;
      },
      error => {
        console.log(error);
        this.isLoading = false;
        // this.loadingDocuments = false;
      }
    );

  }

  selectCard(item) {
    this.selectedcard.emit(item);
  }







}

