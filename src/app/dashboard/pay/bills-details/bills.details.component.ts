import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { startWith, map, first } from 'rxjs/operators';
import { NzNotificationService } from 'ng-zorro-antd';
import { HttpClient } from '@angular/common/http';
import { BillsService } from './../../../provider/bills.service';
import { LayoutComponent } from '../../layout/layout.component';

@Component({
  selector: 'app-bills-details',
  templateUrl: './bills.details.component.html',
  styleUrls: ['./bills.details.component.scss']
})
export class BillsDetailsComponent implements OnInit {
  @ViewChild('closebutton') closebutton:ElementRef;
  formAddBillData: FormGroup;
  isLoading = false;
  isClickable = false;


  // listOfOption = ['Apples', 'Nails', 'Bananas', 'Helicopters'];
  // listOfSelectedValue: string[] = ['Apples', 'Bananas'];


  selectedValue = null;
  listOfOption: Array<{ value: string; text: string }> = [];
  nzFilterOption = (): boolean => true;
  isValid = true;

  constructor(public router: Router, private notification: NzNotificationService, private fb: FormBuilder, private billsService: BillsService, private layoutComponent:LayoutComponent) {
  }

  ngOnInit() {
    this.isValid = true;
    this.formAddBillData = this.fb.group({
      vendorName: ['', [Validators.required]],
      billAmount: [null, [Validators.required]],
      PaymentFreq: [null, [Validators.required]],
      billNo: [null, []]
    });
  }


  search(value) {
    this.listOfOption = [];
    this.billsService.getVedors(value).pipe(first())
      .subscribe(
        response => {
          console.log(response);
          if (response.length > 0) {
            response.forEach(element => {
              this.listOfOption.push({
                value: element.id,
                text: element.companyNameEn
              });
            });
          }
        },
        error => {
          console.log(error);
          this.isLoading = false;
          this.notification
            .blank(
              'Login Error',
              'Email Or Password Incorrect!',
              { nzDuration: 5 }
            )
          // this.loadingDocuments = false;
        }
      );
  }
  AddBill() {
    if (this.formAddBillData.valid) {
      this.isLoading = true;
      let data = {
        "billAmount": this.formAddBillData.value.billAmount,
        "paymentFrequency": this.formAddBillData.value.PaymentFreq,
        "billNo": this.formAddBillData.value.billNo,
        "vendorId": this.formAddBillData.value.vendorName
      }
      this.billsService.addBills(data).pipe(first())
      .subscribe(
        response => {
          this.isLoading = false;
          this.isValid = true;
          console.log(response);
          this.clearData();
          this.closebutton.nativeElement.click();
          if (this.layoutComponent.searchValue != ' ') {
            this.layoutComponent.searchValue = ' ';
          }else{
            this.layoutComponent.searchValue = '';
          }
          this.notification
          .blank(
            'Add Bills Sucessfuly',
            'Bills added Sucessfuly!',
            { nzDuration: 5 }
          );
        },
        error => {
          console.log(error);
          this.isLoading = false;
          this.notification
            .blank(
              'Add Bill Error',
              error.error? error.error.title : 'Some Data Not Valid!',
              { nzDuration: 5 }
            )
          // this.loadingDocuments = false;
        }
      );


    }else{
      this.isValid = false;
    }
    console.log(this.formAddBillData);

  }

  clearData(){
    this.formAddBillData.reset();

  }



}

