import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { startWith, map, first } from 'rxjs/operators';
import { NzNotificationService } from 'ng-zorro-antd';
import { BillsCardsComponent } from '../pay/bills-cards/bills.cards.component';
import { AuthenticateService } from 'src/app/provider/authenticate.service';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {

  searchValue = '';
  searchValue2 = '';
  isLoading = false;
  userData ={};
  constructor(public router: Router, private notification: NzNotificationService, private autn: AuthenticateService) {
  }

  ngOnInit() {
    this.getAccount();
  }

  Search(){
    this.searchValue = this.searchValue2
    console.log(this.searchValue);
    
  }
   getAccount(){
    this.isLoading = true;
    this.autn.getAccount().pipe(first())
    .subscribe(
      response => {
        console.log(response);
        this.userData = response;
        this.isLoading = false;
      },
      error => {
        console.log(error);
        if(error.status == 401){
          this.logout();
        }
        this.isLoading = false;
        // this.loadingDocuments = false;
      }
    );

  }
  logout(){
    localStorage.removeItem("IdToken");
    this.router.navigate(['login']);
  }





}

