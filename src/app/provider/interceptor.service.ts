

import { HttpClient, HttpEvent, HttpHandler, HttpHeaders, HttpInterceptor, HttpParams, HttpRequest, HttpResponse} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { PARAMETERS } from '@angular/core/src/util/decorators';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';
@Injectable()
export class InterceptorService implements HttpInterceptor {
  constructor(private router: Router , private http: HttpClient,) {}
  cookieCheckInterval:any;
  baseURL: string = environment.API_URL;
  logoutURL: string = this.baseURL + "/logout";
   intercept(req: HttpRequest<any>, next: HttpHandler):   Observable<HttpEvent<any>> {
       // All HTTP requests are going to go through this method
       console.log(req)
       if(req && req.headers && localStorage.getItem('IdToken')){
        const authReq = req.clone({
          headers: req.headers.set('Authorization','Bearer '+ localStorage.getItem('IdToken'))
        });
        return next.handle(authReq);
       } else {
        const authReq = req.clone();
         return next.handle(authReq);
       }

    }


    // async logout(sessionExpiredFlag) {
    //   if(this.router.url !== '/singleSignOn/true' && this.router.url !== '/singleSignOn'){
    //     await this.http.get(this.logoutURL).toPromise();
    //     if(sessionExpiredFlag){
    //       this.router.navigate(['/singleSignOn','true']);
    //     } else {
    //       this.router.navigate(['/singleSignOn']);
    //     }
    //   }
    // }
}
