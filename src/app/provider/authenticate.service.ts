import { Injectable } from "@angular/core";
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/internal/operators/map';

@Injectable()
export class AuthenticateService {
  private baseURL: string = environment.API_URL;
  constructor(private http: HttpClient) {}

  authenticateSerives() {
    const t_requestURL = this.baseURL + "authenticate";
    return this.http
      .get<any>(t_requestURL)
      .pipe(
        map(response => {
          return response;
        })
      );
  }
  loginSerives(payload) {
    const t_requestURL = this.baseURL + "authenticate";
    return this.http
      .post<any>(t_requestURL, payload)
      .pipe(
        map(response => {
          return response;
        })
      );
  }

  registerSerives(payload) {
    const t_requestURL = this.baseURL + "register-user";
    return this.http.post<any>(t_requestURL, payload)
      .pipe(
        map(response => {
          return response;
        })
      );
  }

  getAccount() {
    const t_requestURL = this.baseURL + "account";
    return this.http
      .get<any>(t_requestURL)
      .pipe(
        map(response => {
          return response;
        })
      );
  }

    forgetSerives(email) {
        const t_requestURL = this.baseURL + "account/reset-password/init";
        return this.http.post<any>(t_requestURL, email);
    }

    resetPassword(key: string, newPassword: string){
        const t_requestURL = this.baseURL + "account/reset-password/finish";
        return this.http.post(t_requestURL, { key, newPassword });
    }
}
