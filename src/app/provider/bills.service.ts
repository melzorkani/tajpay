import { Injectable } from "@angular/core";
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/internal/operators/map';

@Injectable()
export class BillsService {
  private baseURL: string = environment.API_URL;
  constructor(private http: HttpClient) {}

  getVedors(value) {
    const t_requestURL = this.baseURL + "user-profiles?companyNameEn.contains="+value+"&page=0&size=100";
    return this.http
      .get<any>(t_requestURL)
      .pipe(
        map(response => {
          return response;
        })
      );
  }

  addBills(payload) {
    const t_requestURL = this.baseURL + "transactions";
    return this.http.post<any>(t_requestURL, payload)
      .pipe(
        map(response => {
          return response;
        })
      );
  }

  getTransaction(key , value) {
    const t_requestURL = this.baseURL + "transaction?key="+key+"&search="+value;
    return this.http
      .get<any>(t_requestURL)
      .pipe(
        map(response => {
          return response;
        })
      );
  }
}
