import {AfterViewInit, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {AuthenticateService} from "../provider/authenticate.service";
import {NzNotificationService} from "ng-zorro-antd";
import {first} from "rxjs/operators";

@Component({
  selector: 'app-forget-password',
  templateUrl: './forget-password.component.html',
  styleUrls: ['./forget-password.component.scss']
})
export class ForgetPasswordComponent implements OnInit, AfterViewInit {
  @ViewChild('email')
  email?: ElementRef;

  success = false;
  resetRequestForm = this.fb.group({
    email: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(254), Validators.email]],
  });

  isLoading = false;
  isClickable = false;
  constructor(public router: Router, private authenticateService:AuthenticateService, private notification: NzNotificationService, private fb: FormBuilder) {
  }

  ngOnInit() {
  }

  ngAfterViewInit(): void {
    if (this.email) {
      this.email.nativeElement.focus();
    }
  }

  emailIsChanged(){
    this.isClickable = true;
  }

  forget() {
    this.isClickable = true;
    if (this.resetRequestForm.valid) {
      this.isLoading = true;
      let body = this.resetRequestForm.value.email;
      this.authenticateService.forgetSerives(body).pipe(first())
          .subscribe(
              response => {
                this.success = true;
                this.isLoading=false;
              }
              ,error => {
                console.log(error);
                this.isLoading = false;
                this.success=false;
                this.notification
                    .blank(
                        'Forget-Password Error',
                        error.title,
                        { nzDuration: 5 }
                    )
              }
          )
    }
  }
}
