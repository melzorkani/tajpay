import { Component, OnInit, AfterViewInit, ElementRef, ViewChild } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {AuthenticateService} from "../provider/authenticate.service";
import {NzNotificationService} from "ng-zorro-antd";

@Component({
  selector: 'app-reset-finish',
  templateUrl: './reset-finish.component.html',
  styleUrls: ['./reset-finish.component.scss']
})
export class ResetFinishComponent implements OnInit, AfterViewInit {
  @ViewChild('newPassword')
  newPassword?: ElementRef;

  initialized = false;
  doNotMatch = false;
  error = false;
  success = false;
  key = '';
  isLoading = false;
  isClickable = false;

  passwordForm = this.fb.group({
    newPassword: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(50)]],
    confirmPassword: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(50)]],
  });

  constructor(private authenticateService:AuthenticateService, private route: ActivatedRoute, private fb: FormBuilder, private notification: NzNotificationService) {
  }

  ngOnInit(): void {
    this.route.queryParams.subscribe(params => {
      if (params['key']) {
        this.key = params['key'];
      }
      this.initialized = true;
    });
  }

  ngAfterViewInit(): void {
    if (this.newPassword) {
      this.newPassword.nativeElement.focus();
    }
  }

  finishReset(): void {
    this.doNotMatch = false;
    this.error = false;
    this.isLoading = true;
    this.isClickable = true;


    const newPassword = this.passwordForm.get(['newPassword'])!.value;
    const confirmPassword = this.passwordForm.get(['confirmPassword'])!.value;

    if (newPassword !== confirmPassword) {
      this.doNotMatch = true;
    } else {
      this.authenticateService.resetPassword(this.key, newPassword).subscribe({
        next: () => {
          this.success = true;
          this.isLoading=false;
        },
        error: err => {
          console.log(err);
          this.error = true
          this.isLoading = false;
          this.notification
              .error(
                  'Password Reset Error',
                  'Your password couldn\'t be reset. Remember a password request is only valid for 24 hours.',
                  { nzDuration: 5 }
              )
        },
      });
    }
  }

}
