import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResetFinishComponent } from './reset-finish.component';

describe('ResetFinishComponent', () => {
  let component: ResetFinishComponent;
  let fixture: ComponentFixture<ResetFinishComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResetFinishComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResetFinishComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
