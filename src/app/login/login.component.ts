import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthenticateService } from '../provider/authenticate.service';
import { startWith, map, first } from 'rxjs/operators';
import { NzNotificationService } from 'ng-zorro-antd';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  validateForm: FormGroup;

  data = {
    email : '',
    password: ''
  }
  formLoginData: FormGroup;
  isLoading = false;
  isClickable = false;
  constructor(public router: Router, private authenticateService:AuthenticateService, private notification: NzNotificationService, private fb: FormBuilder) {
  }

  ngOnInit() {
    this.formLoginData = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: [null, [Validators.required]],
      rememberMe: [false, []],
   });

  }

  signUp(){
    this.router.navigate(['login']);
  }
  emailIsChanged(){
    this.isClickable = true;
  }

  login(){
    this.isClickable = true;
    if (this.formLoginData.valid) {
    this.isLoading = true;
    let body = {
      "username":this.formLoginData.value.email ,
      "password": this.formLoginData.value.password,
      "rememberMe": this.formLoginData.value.rememberMe
    }
    this.authenticateService.loginSerives(body).pipe(first())
      .subscribe(
        response => {
          console.log(response);
          localStorage.setItem("IdToken", response.id_token)
          this.isLoading = false;
          this.router.navigate(['dashboard']);
        },
        error => {
          console.log(error);
          this.isLoading = false;
          this.notification
          .blank(
            'Login Error',
            'Email Or Password Incorrect!',
            { nzDuration: 5 }
          )
          // this.loadingDocuments = false;
        }
      );
      }


  }

}

